<?php
/**
 * @file
 * Cache Mover backend.
 */

class CacheMover implements DrupalCacheInterface {

  protected $bin;

  protected $src_bin;
  protected $dst_bin;

  protected $src_class;
  protected $dst_class;

  protected $src_cache;
  protected $dst_cache;

  protected $delete_from_src_on_get;
  protected $delete_from_src_on_set;

  private $var_prefix = 'cache_mover_';

  function __construct($bin) {
    $this->bin = $bin;

    $this->src_bin = $this->variable_get('src_bin', '{bin}');
    $this->src_bin = str_replace('{bin}', $bin, $this->src_bin);
    $this->dst_bin = $this->variable_get('dst_bin', '{bin}');
    $this->dst_bin = str_replace('{bin}', $bin, $this->dst_bin);

    // @todo: Test this code below.
    $default_cache_class = variable_get('cache_default_class', 'DrupalDatabaseCache');
    if ($default_cache_class === get_class($this)) {
      // Avoid an infinite loop in case CacheMover is configured as the
      // default cache backend.
      $default_cache_class = 'DrupalDatabaseCache';
    }
    $this->src_class = $this->variable_get('src_class', $default_cache_class);
    $this->dst_class = $this->variable_get('dst_class', $default_cache_class);

    $this->src_cache = new $this->src_class($this->src_bin);
    $this->dst_cache = new $this->dst_class($this->dst_bin);

    $this->delete_from_src_on_get = $this->variable_get('delete_from_src_on_get', TRUE);
    $this->delete_from_src_on_set = $this->variable_get('delete_from_src_on_set', TRUE);
  }

  /**
   * Overrides DrupalCacheInterface::get().
   */
  function get($cid) {
    // It is necessary to create a variable to allow pass by reference.
    $cids = array($cid);
    $cache = $this->getMultiple($cids);
    return reset($cache);
  }

  /**
   * Overrides DrupalCacheInterface::getMultiple().
   */
  function getMultiple(&$cids) {
    $cache = array();

    $cache = $this->dst_cache->getMultiple($cids);
    // If there is a cache hit for even a single cid, consider it a
    // hit on destination cache.
    if (count($cache) > 0) {
      /* Found items on destination cache are deleted from source cache by
       * default to avoid them to be found later if expiration happens with
       * different TTLs between source and destination bin.
       */
      if ($this->delete_from_src_on_get) {
        $this->src_cache->clear(array_keys($cache));
      }
      return $cache;
    }

    $cache = $this->src_cache->getMultiple($cids);
    return $cache;
  }

  function set($cid, $data, $expire = CACHE_PERMANENT) {
    // Delete updated items from source cache to avoid them to be found if
    // expirations are different between source and destination caches.
    // We're trading speed for safety here. Mixed expiration times and/or cache
    // backends might have unpredictable results.
    if ($this->delete_from_src_on_set) {
      $this->src_cache->clear($cid);
    }
    return $this->dst_cache->set($cid, $data, $expire);
  }

  function clear($cid = NULL, $wildcard = FALSE) {
    // Always delete on source bin to avoid cache hits on old items
    // in the source bin after a cache miss on destination bin.
    $this->src_cache->clear($cid, $wildcard);
    return $this->dst_cache->clear($cid, $wildcard);
  }

  function isEmpty() {
    return $this->src_cache->isEmpty() && $this->dst_cache->isEmpty();
  }

  public function getSrcBin() {
    return $this->src_bin;
  }

  public function getDstBin() {
    return $this->dst_bin;
  }

  public function getSrcClass() {
    return $this->src_class;
  }

  public function getDstClass() {
    return $this->dst_class;
  }

  public function getSrcCacheObject() {
    return $this->src_cache;
  }

  public function getDstCacheObject() {
    return $this->dst_cache;
  }

  public function isDeleteFromSourceOnSetEnabled() {
    return $this->delete_from_src_on_set;
  }

  public function isDeleteFromSourceOnGetEnabled() {
    return $this->delete_from_src_on_get;
  }

  /**
   * Get a variable for the class specific bin, fallback to 'default' bin.
   *
   * Drupal API original variable_get:
   * function variable_get($name, $default = NULL) {
   *   global $conf;
   *
   *   return isset($conf[$name]) ? $conf[$name] : $default;
   * }
   *
   * @see variable_get
   */
  function variable_get($name, $default = NULL, $usefallback = TRUE) {

    // Get variable for class bin. Do not honor $default yet to allow
    // 'default' bin fallback, if requested.
    $data = variable_get($this->var_prefix . $this->bin . '_' . $name);

    // Let the $usefallback function parameter control the default value
    // behaviour by now. Code should be reviewed later to check if still
    // makes sense. As of now, leave this as a self-remider.
    if (!isset($data)) {
      if ($usefallback) {
        return variable_get($this->var_prefix . 'default_' . $name, $default);
      } else {
        return $default;
      }
    }
    return $data;
  }
}
